# Solutions of the exercise about the Flask web server serving pet names
## Run the script in a venv

```bash
python -m venv myenv
source myenv/bin/activate
pip install -r requirements.txt
python -m flask --app run petname-app --debug
```

`--debug` activates hot reload and prints additional debugging informations.

