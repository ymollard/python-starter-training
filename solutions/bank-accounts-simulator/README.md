# Solutions of the bank account exercise

This is a proposed solution to the exercises about the bank account package.

## Run unit tests via `tox`

```bash
pip install tox
tox .
```

## Install and run regular script for scenario in a venv

```bash
python -m venv myenv
source myenv/bin/activate

pip install .
python scenario.py
```
