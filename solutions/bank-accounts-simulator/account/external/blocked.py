from typing import Optional
from datetime import datetime
from ..internal import BankAccount


class InsufficientBalance(ValueError):
    pass


class BlockedBankAccount(BankAccount):
    def transfer_to(self, recipient: "BankAccount", value: int, transaction_date: Optional[datetime]) -> None:
        if value > self.balance:
            raise InsufficientBalance(f"Balance {self.balance} of {self.owner} if not enough for a ${value} transfer")
        super().transfer_to(recipient, value, transaction_date)

