from typing import Optional
from datetime import datetime
from .monitor import monitor


class BankAccount:
    def __init__(self, owner: str, initial_balance: int):
        self.__owner: str = owner
        self._balance: int = initial_balance

    def __str__(self):
        return f"Account of {self.__owner} with a balance of ${self._balance}"

    @property
    def balance(self):
        return self._balance

    @property
    def owner(self):
        return self.__owner

    @owner.setter
    def owner(self, new_owner: str):
        self.__owner = new_owner

    def _credit(self, value: float, transaction_date: Optional[datetime]) -> None:
        self._balance += value

    @monitor
    def transfer_to(self, recipient: "BankAccount", value: int, transaction_date: Optional[datetime]) -> None:
        self._balance -= value
        recipient._credit(value, transaction_date)

