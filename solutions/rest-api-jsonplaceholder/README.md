# Solutions of the exercise about the download from the jsonplacehodler API
## Run the script in a venv

```bash
python -m venv myenv
source myenv/bin/activate
pip install -r requirements.txt
```

And then execute the script corresponding to the desired operation:
```bash
python main_part1_get.py
python main_part2_post.py
python main_part3_delete.py
python main_part4_delphine.py
```

Note that POST and DELETE operations do not actually modify the database.
