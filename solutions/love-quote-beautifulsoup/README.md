# Solutions of the exercise about the extraction of love quotes with bs4
## Run the script in a venv

```bash
python -m venv myenv
source myenv/bin/activate
pip install -r requirements.txt
python love_quote.py
```
