from typing import Optional
from datetime import datetime
from ..internal import BankAccount


class AgiosBankAccount(BankAccount):
    def __init__(self, owner: str, initial_balance: int, bank_account: "BankAccount"):
        super().__init__(owner, initial_balance)
        self.my_bank: BankAccount = bank_account
        self.negative_from: Optional[datetime] = None

    def __check_for_agios(self, current_date: datetime):
        if self.negative_from is not None:
            difference = current_date - self.negative_from
            agios = int(difference.days)
            if agios > 0:
                self.negative_from = None
                self.transfer_to(self.my_bank, agios, current_date)

    def transfer_to(self, recipient: "BankAccount", value: int, transaction_date: Optional[datetime]) -> None:
        super().transfer_to(recipient, value, transaction_date)
        if self.balance < 0 and self.negative_from is None:
            self.negative_from = transaction_date

    def _credit(self, value: float, transaction_date: Optional[datetime]):
        super()._credit(value, transaction_date)
        self.__check_for_agios(transaction_date)

