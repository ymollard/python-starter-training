import requests

url = 'https://jsonplaceholder.typicode.com/posts'

response = requests.get(url)

if response.ok:
    posts = response.json()

    for i in range(50):
        print(f"Post {i+1} Title: {posts[i]['title']}")
else:
    print(f"Failed to retrieve posts. Status code: {response.status_code}")
