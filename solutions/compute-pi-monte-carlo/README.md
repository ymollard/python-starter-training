# Solutions of the exercise about computation of PI by Monte-Carlo
## Run the script in a venv

```bash
python -m venv myenv
source myenv/bin/activate
python compute_pi_monte_carlo.py
```
