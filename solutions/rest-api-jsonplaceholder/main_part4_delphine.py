import requests

users_url = 'https://jsonplaceholder.typicode.com/users'
users_response = requests.get(users_url)

if users_response.ok:
    users = users_response.json()

    delphine_user = None
    for user in users:
        if user['username'] == 'Delphine':
            delphine_user = user
            break

    if delphine_user:
        user_id = delphine_user['id']
        print(f"User ID for 'Delphine': {user_id}")

        albums_url = f'https://jsonplaceholder.typicode.com/users/{user_id}/albums'
        albums_response = requests.get(albums_url)

        if albums_response.ok:
            albums = albums_response.json()

            if albums:
                first_album_id = albums[0]['id']
                print(f"First album ID for user 'Delphine': {first_album_id}")

                photos_url = f'https://jsonplaceholder.typicode.com/albums/{first_album_id}/photos'
                photos_response = requests.get(photos_url)

                if photos_response.ok:
                    photos = photos_response.json()
                    print(f"Photos for the first album (ID: {first_album_id}):")
                    for photo in photos:
                        print(f"Photo ID: {photo['id']}, Title: {photo['title']}, URL: {photo['url']}")
                else:
                    print(f"Failed to retrieve photos. Status code: {photos_response.status_code}")
            else:
                print("No albums found for the user 'Delphine'")
        else:
            print(f"Failed to retrieve albums. Status code: {albums_response.status_code}")
    else:
        print("User with the username 'Delphine' not found")
else:
    print(f"Failed to retrieve users. Status code: {users_response.status_code}")
