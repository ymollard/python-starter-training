import requests

# Fill with the post ID to be deleted
post_id = 2

delete_url = f'https://jsonplaceholder.typicode.com/posts/{post_id}'
delete_response = requests.delete(delete_url)

if delete_response.ok:
    print(f"Blog post with ID {post_id} has been successfully deleted.")
else:
    print(f"Failed to delete the blog post with ID {post_id}. Status code: {delete_response.status_code}")
