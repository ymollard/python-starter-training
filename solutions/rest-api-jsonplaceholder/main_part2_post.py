import requests

post_url = 'https://jsonplaceholder.typicode.com/posts'

new_post = {
    'title': 'Python training',
    'body': 'I am training to the requests module with Python',
    'userId': 1
}

post_response = requests.post(post_url, json=new_post)

if post_response.ok:
    new_post_id = post_response.json()['id']
    print(f"New post created with ID: {new_post_id}")
else:
    print(f"Failed to create a new post. Status code: {post_response.status_code}")
