#!/usr/bin/env python
from random import random

# 1. Generate n couples of (x, y) points
n = 1000000    # Number of matches (x and y coordinates): larger "n" will give more precise result
x_values = [random() for _ in range(n)]
y_values = [random() for _ in range(n)]

# 2. and 3. Count how many matches are inside and outside the 1/4 of circle
m = 0  # Number of the "n" points inside the 1/4 of circle
for x, y in zip(x_values, y_values):
    if x**2 + y**2 < 1:
        m += 1

# Get the area of the 1/4 of circle and deduce PI by multipyling by 4
area = m/n
pi = 4*area

print("Estimated value of Pi:", pi)

# 5. Plot the green and red points (may take a while because points are numerous)
from matplotlib import pyplot

green_points = []  # list of 2-tuples e.g. [(42, 54), (34, 75), ...]
red_points = []    # list of 2-tuples e.g. [(42, 54), (34, 75), ...]
for x, y in zip(x_values, y_values):
    if x**2 + y**2 < 1:
        green_points.append((x, y))   # append a new 2-tuple
    else:
        red_points.append((x, y))     # append a new 2-tuple

pyplot.scatter([m[0] for m in green_points], [m[1] for m in green_points], color='green')
pyplot.scatter([m[0] for m in red_points], [m[1] for m in red_points], color='red')
pyplot.axis('equal')
pyplot.show()   # show() actually displays the ongoing plot
