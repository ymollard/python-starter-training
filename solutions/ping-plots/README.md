# Solutions of the exercise about Ping plots
## Run the script in a venv

```bash
python -m venv myenv
source myenv/bin/activate
pip install -r requirements.txt
python ping_plots.txt
```
