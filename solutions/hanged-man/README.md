# Solutions of the exercise about Hanged Man
## Run the script in a venv

```bash
python -m venv myenv
source myenv/bin/activate
python hanged_man.py
```
