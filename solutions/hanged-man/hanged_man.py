"""
This is a simple hanged man game running in the console
"""
from random import choice

# words: a list of possible words to be guessed
words = ["computer", "correction", "mouse", "keyboard", "screen", "finance", "mountain", "beach", "beetle"]
# secret_word: a secret word randomly picked among the previous list (use for instance random.choice )
secret_word = choice(words).lower()
# displayed_word: the partially hidden word, i.e. the word of same length as the secret word in which every letter is replaced by an underscore
displayed_word = len(secret_word) * "_"
# remaining_attempts: the number of remaining attempts. Initialize it to the number of letters in secret_word
remaining_attempts = len(secret_word) + 2


def input_letter() -> str:
    user_input: str = input("Type a letter to unveil: ")
    if len(user_input) == 1 and user_input.isalpha():
        return user_input.lower()
    else:
        return input_letter()


def unveil(letter: str, original_word: str, hidden_word: str) -> str:
    new_hidden_word: str = ""
    for i, v in enumerate(hidden_word):
        if letter == original_word[i]:
            new_hidden_word += letter
        else:
            new_hidden_word += hidden_word[i]
    return new_hidden_word

# Some assertions that test the function call in all possible contexts
# assert statements are silent if the tests return True
assert unveil("l", "hello", "_____") == "__ll_"
assert unveil("l", "hello", "____o") == "__llo"
assert unveil("x", "hello", "____o") == "____o"

# The main game loop
while True:
    # Displays the partially hidden word and the number of remaining attempts
    print(f"You must guess {displayed_word} is less of {remaining_attempts + 1} attempts")
    # Prompts the player to enter a valid letter with input_letter()
    letter: str = input_letter()
    # Replaces matches of this letter from secret_word in displayed_word , if any
    displayed_word = unveil(letter, secret_word, displayed_word)
    # Checks the game state: exit the program with an appropriate message if the player wins or looses
    if remaining_attempts == 0:
        print(f"You lost! The secret word was {secret_word}")
        break
    elif secret_word == displayed_word:
        print(f"You won! The secret word was {secret_word}")
        break
    else:
        remaining_attempts -= 1
